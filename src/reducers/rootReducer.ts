import {AnyAction, Reducer} from 'redux';

import {Post} from '../interfaces/Post';
import {State} from '../interfaces/State';

import {search} from '../services/FullTextSearchService';
import {makeInstant} from '../utils/postVisibilityHandlers';

import {Actions} from './Actions';

export const rootReducer: Reducer<State, AnyAction> =
    (state: State, action: AnyAction) => {
        switch (action.type) {
            case Actions.CLEAR_POSTS:
                return {...state, posts: [], applicationError: '', pattern: ''};

            case Actions.REPORT_APPLICATION_ERROR:
                return {...state, posts: [], applicationError: action.value.toString()};

            case Actions.UPDATE_POSTS:
                return {...state, posts: action.value, applicationError: ''};

            case Actions.SEARCH:
                return {...state, posts: search(action.value, state.posts), pattern: action.value};

            case Actions.SHOW_AFTER_DELAY:
                return {...state, posts: turnVisible(state.posts, action.value)};

            default:
                return state;
        }
    };

const turnVisible = (posts: Post[], id: number): Post[] => {
    const newPosts = [...posts];
    const affectedPostIndex = newPosts.findIndex((post) => post.id === id);

    if (affectedPostIndex) {
        newPosts[affectedPostIndex] = makeInstant(newPosts[affectedPostIndex]);
    }

    return newPosts;
};
