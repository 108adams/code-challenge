export const Actions = {
    CLEAR_POSTS: 'clear_posts',
    REPORT_APPLICATION_ERROR: 'report_fetch_error',
    SEARCH: 'search',
    SHOW_AFTER_DELAY: 'show_after_delay',
    START_DELAYED_PRESENTATION: 'start_delayed_presentation',
    UPDATE_POSTS: 'update_posts',
};
