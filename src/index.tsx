import * as React from 'react';
import {render} from 'react-dom';
import {applyMiddleware, compose, createStore} from 'redux';
import createSagaMiddleware, {SagaMiddleware} from 'redux-saga';

import {Root} from './components/Root/Root';
import {State} from './interfaces/State';
import {rootReducer} from './reducers/rootReducer';
import {rootSaga} from './sagas/rootSaga';

import 'bootstrap/dist/css/bootstrap.css';
import './index.less';

const getInitialState = (): State => ({
    applicationError: '',
    pattern: '',
    posts: [],
});

const getStore = () => {
    // in real life this would be turned on only in dev build
    // @see https://github.com/zalmoxisus/redux-devtools-extension
    const composeEnchancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const sagaMiddleware: SagaMiddleware<any> = createSagaMiddleware();

    const store = createStore(
        rootReducer,
        getInitialState(),
        composeEnchancers(applyMiddleware(sagaMiddleware)),
    );

    sagaMiddleware.run(rootSaga);

    return store;
};

render(
    <Root store={getStore()}/>,
    document.getElementById('root') as HTMLElement,
);
