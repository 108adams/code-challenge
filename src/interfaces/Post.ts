import {RawPost} from './RawPost';

// mapped type: make all properties of T obligatory
type Required<T> = {
    [P in keyof T]: T[P];
};

export type Post = Required<RawPost>;
