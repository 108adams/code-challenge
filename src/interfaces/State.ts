import {Post} from './Post';

export interface State {
    // global error (connectivity)
    applicationError: string;

    // search pattern for Full Text Search
    pattern: string;

    posts: Post[];
}
