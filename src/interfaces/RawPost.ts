export interface RawPost {
    id: number;
    userId: number;

    title: string;
    body: string;

    isDelayed?: boolean;
    isVisible?: boolean;
}
