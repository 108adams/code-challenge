import {delay} from 'redux-saga';
import {call, CallEffect, ForkEffect, put, PutEffect, select, SelectEffect, takeEvery} from 'redux-saga/effects';

import {Action} from '../interfaces/Action';
import {Actions} from '../reducers/Actions';
import {findNextPostId, getFirstPostId, getPosts} from './stateHelpers';

const DELAY = 1000;

/**
 * When data arrives (Actions.UPDATE_POSTS) we start showing the list with the first element
 *
 * Then every time a post is shown we wait 1s and show next one.
 *
 * Stop when there are no more posts to show.
 */
export function* delaySaga(): IterableIterator<ForkEffect> {
    yield takeEvery<Action>(
        [
            Actions.SHOW_AFTER_DELAY,
            Actions.UPDATE_POSTS,
        ],
        delayActionHandler,
    );
}

function* delayActionHandler(action: Action): IterableIterator<PutEffect<Action> | CallEffect | SelectEffect> {
    switch (action.type) {

        case Actions.CLEAR_POSTS:
            // stop timer by passing invalid id
            yield put({type: Actions.SHOW_AFTER_DELAY, value: -1});
            break;

        case Actions.UPDATE_POSTS:
            yield* showFirstPost();
            break;

        case Actions.SHOW_AFTER_DELAY:
            yield* showAfterDelay(action);
            break;

        default:
            break;
    }
}

function* showFirstPost() {
    const posts = yield select(getPosts);
    yield put({type: Actions.SHOW_AFTER_DELAY, value: getFirstPostId(posts)});
}

function* showAfterDelay(action: Action): IterableIterator<PutEffect<Action> | CallEffect | SelectEffect> {
    const posts = yield select(getPosts);
    const nextId = findNextPostId(posts, action.value);

    if (nextId !== -1) {
        yield call(delay, DELAY);
        yield put({type: Actions.SHOW_AFTER_DELAY, value: findNextPostId(posts, action.value)});
    }
}
