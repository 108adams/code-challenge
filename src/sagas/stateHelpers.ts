import {Post} from '../interfaces/Post';
import {State} from '../interfaces/State';

export const getPosts = (state: State): Post[] =>
    state.posts;

export const getFirstPostId = (posts: Post[] = []) =>
    getPostIds(posts)[0];

export const findNextPostId = (posts: Post[], currentId: number): number => {
    const postIds = getPostIds(posts);

    const nextId = postIds.indexOf(currentId) + 1;

    if (nextId === 0) {
        return -1; // end the iteration, non-existent id provided
    }

    if (postIds[nextId]) {
        return postIds[nextId];
    }

    return -1; // undefined at index: end of collection reached
};

const getPostIds = (posts: Post[]) =>
    posts
        .map((post) => post.id)
        .sort(comparator);

const comparator = (a: number, b: number): number =>
    a - b;
