import {ForkEffect} from 'redux-saga/effects';
import {delaySaga} from './delaySaga';

/**
 * @link https://redux-saga.js.org/
 */
export const rootSaga = function*(): IterableIterator<Array<IterableIterator<ForkEffect>>> {
    yield [delaySaga()];
};
