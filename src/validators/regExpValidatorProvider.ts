import {ValidatorProvider} from './interfaces/ValidatorProvider';
import {RegExpValidatorConfiguration} from './interfaces/validators/RegExpValidatorConfiguration';

export const regExpValidatorProvider: ValidatorProvider =
    (message, {regExp}: RegExpValidatorConfiguration) =>
        (value: string) =>
            regExp.test(value)
                ? ''
                : message;
