import {ValidationResult} from './interfaces/ValidationResult';
import {Validator} from './interfaces/Validator';

export const executeValidators = (validators: Validator[] = []) =>
    (value: string): ValidationResult[] =>
        validators
            .map((validator) => validator(value))
            .filter((error) => !!error);
