import {Validator} from './Validator';

export type ValidatorProvider = (message: string, options?: unknown) => Validator;
