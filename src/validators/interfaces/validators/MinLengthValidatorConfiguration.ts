export interface MinLengthValidatorConfiguration {
    minLength: number;
}
