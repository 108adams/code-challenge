import {ValidationResult} from './ValidationResult';

export type Validator = (value: any) => ValidationResult;
