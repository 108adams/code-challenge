import {ValidatorProvider} from './interfaces/ValidatorProvider';
import {MinLengthValidatorConfiguration} from './interfaces/validators/MinLengthValidatorConfiguration';

export const minLengthValidatorProvider: ValidatorProvider =
    (message, {minLength}: MinLengthValidatorConfiguration) =>
        (value: string) =>
            value.length >= minLength
                ? ''
                : message;
