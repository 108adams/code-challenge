import * as React from 'react';
import {connect} from 'react-redux';

import {State as ReduxState} from '../../interfaces/State';
import {ErrorModal} from './components/ErrorModal';

interface StateProps {
    applicationError: string;
}

interface ComponentProps {
    exitLink: string;
    exitLabel: string;
}

// double export: we use this component both with redux store and without any
export const Component = ({applicationError, exitLink, exitLabel}: ComponentProps & StateProps) =>
    applicationError
        ? <ErrorModal errorMessage={applicationError} {...{exitLink, exitLabel}}/>
        : null;

const mapStateToProps = (state: ReduxState): StateProps => ({
    applicationError: state.applicationError,
});

export const ApplicationError = connect(mapStateToProps)(Component);
