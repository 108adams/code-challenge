import * as React from 'react';
import {Modal} from 'react-bootstrap';
import {Link} from 'react-router-dom';

interface ErrorModalProps {
    errorMessage: string;
    exitLink: string;
    exitLabel: string;
}

export const ErrorModal = ({errorMessage, exitLink, exitLabel}: ErrorModalProps) =>
    (
        <div className="static-modal">
            <Modal.Dialog>
                <Modal.Header>
                    <Modal.Title>Error </Modal.Title>
                </Modal.Header>

                <Modal.Body> {errorMessage} </Modal.Body>

                <Modal.Footer>
                    <Link className="btn btn-info" to={exitLink}>{exitLabel}</Link>
                </Modal.Footer>
            </Modal.Dialog>
        </div>
    );
