export interface FieldValue {
    errors: string[];
    value: string;
}
