import {FieldValue} from './FieldValue';

export interface State {
    isLoginErrorVisible: boolean;
    isUserLoggedIn: boolean;
    password: FieldValue;
    username: FieldValue;
}
