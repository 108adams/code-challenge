import {FormControlProps} from 'react-bootstrap/lib/FormControl';

import {ValidationResult} from '../../../validators/interfaces/ValidationResult';

export type UpdateFormCallback = (
    event: React.FormEvent<FormControlProps>,
    errors: ValidationResult[],
) => void;
