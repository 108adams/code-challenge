import {UpdateFormCallback} from './UpdateFormCallback';

export interface FieldProps {
    value: string;

    updateFormState: UpdateFormCallback;
}
