import * as React from 'react';
import * as ReactDOM from 'react-dom';

import {Login} from './Login';

// An example simple test of redux-less component
describe('Root component', () => {

    it('renders without crashing', () => {
        // GIVEN
        const div = document.createElement('div');

        // THEN
        ReactDOM.render(<Login/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});
