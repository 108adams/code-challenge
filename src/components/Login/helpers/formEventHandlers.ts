import * as React from 'react';
import {FormControlProps} from 'react-bootstrap/lib/FormControl';

import {UpdateFormCallback} from '../interfaces/UpdateFormCallback';

import {executeValidators} from '../../../validators/executeValidators';
import {Validator} from '../../../validators/interfaces/Validator';

export const handleRaw =
    (onChangeParentCallback: UpdateFormCallback) =>
        (event: React.FormEvent<FormControlProps>): void =>
            onChangeParentCallback(event, []);

export const handleWithValidation =
    (validators: Validator[], onChangeParentCallback: UpdateFormCallback) =>
        (event: React.FormEvent<FormControlProps>): void => {
            const value = (event.currentTarget.value  as string);
            onChangeParentCallback(event, executeValidators(validators)(value));
        };
