import * as React from 'react';
import {ControlLabel, FormControl, FormControlProps, FormGroup} from 'react-bootstrap';

import './Input.less';

export interface InputProperties {
    label: string;
    placeholder: string;
    value: string;

    type?: 'text' | 'password';

    onChange: React.EventHandler<React.FormEvent<FormControlProps>>;
    onUpdate: React.EventHandler<React.FormEvent<FormControlProps>>;
}

export const Input =
    ({
         type = 'text',
         label,
         placeholder,
         value,

         onChange,
         onUpdate,
     }: InputProperties) =>
        (
            <FormGroup>
                <ControlLabel>
                    {label}
                </ControlLabel>

                <FormControl
                    type={type}
                    placeholder={placeholder}
                    value={value}
                    onChange={onChange}
                    onBlur={onUpdate}
                />
            </FormGroup>
        );
