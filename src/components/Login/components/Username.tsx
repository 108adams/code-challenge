import * as React from 'react';

import {minLengthValidatorProvider} from '../../../validators/minLengthValidatorProvider';
import {handleRaw, handleWithValidation} from '../helpers/formEventHandlers';

import {FieldProps} from '../interfaces/FieldProps';

import {Input} from './Input';

const MIN_USERNAME_LENGTH = 5;

const VALIDATORS = [
    minLengthValidatorProvider(
        `Username must be at least ${MIN_USERNAME_LENGTH} characters long`,
        {minLength: MIN_USERNAME_LENGTH},
    ),
];

export const Username = ({value, updateFormState}: FieldProps) =>
    (
        <Input
            label="Username"
            placeholder="mylogin"
            type="text"
            value={value}

            onChange={handleRaw(updateFormState)}
            onUpdate={handleWithValidation(VALIDATORS, updateFormState)}
        />
    );
