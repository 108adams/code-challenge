import * as React from 'react';

import {Validator} from '../../../validators/interfaces/Validator';
import {minLengthValidatorProvider} from '../../../validators/minLengthValidatorProvider';
import {regExpValidatorProvider} from '../../../validators/regExpValidatorProvider';

import {handleRaw, handleWithValidation} from '../helpers/formEventHandlers';
import {FieldProps} from '../interfaces/FieldProps';
import {Input} from './Input';

// source https://stackoverflow.com/questions/19605150/
const VALID_PASSWORD_REGEXP = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)[A-Za-z\d]{3,}$/;
const MIN_PASSWORD_LENGTH = 8;

const VALIDATORS: Validator[] = [
    minLengthValidatorProvider(
        `Password must be at least ${MIN_PASSWORD_LENGTH} characters long`,
        {minLength: MIN_PASSWORD_LENGTH},
    ),
    regExpValidatorProvider(
        'Password must contain at least one small letter, at least one capital letter, at least one number',
        {regExp: VALID_PASSWORD_REGEXP},
    ),
];

export const Password = ({value, updateFormState}: FieldProps) =>
    (
        <Input
            label="Password"
            placeholder="Password123"
            type="password"
            value={value}

            onChange={handleRaw(updateFormState)}
            onUpdate={handleWithValidation(VALIDATORS, updateFormState)}
        />
    );
