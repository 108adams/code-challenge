import * as React from 'react';
import {Col, FormControlProps, Grid, Row} from 'react-bootstrap';
import {Redirect} from 'react-router';

import * as SessionService from '../../services/SessionService';
import {ValidationResult} from '../../validators/interfaces/ValidationResult';

import {State} from './interfaces/State';

import {generateErrorMessages} from '../../utils/generateErrorMessages';
import {Password} from './components/Password';
import {Username} from './components/Username';

import './Login.less';

export class Login extends React.Component<{}, State> {

    private static readonly LOGIN_ERROR = ['Please provide valid credentials'];

    constructor(props: {}) {
        super(props);

        this.state = {
            isLoginErrorVisible: false,
            isUserLoggedIn: false,
            password: {value: '', errors: []},
            username: {value: '', errors: []},
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.updateFormState = this.updateFormState.bind(this);
    }

    public render() {
        return (
            <Grid className="login">
                <Row className="show-grid">
                    <Col xs={12} md={8} mdOffset={2} lg={6} lgOffset={3}>

                        <h1>Please log in</h1>

                        {this.getForm()}

                        {this.state.isUserLoggedIn && <Redirect to="/"/>}

                    </Col>
                </Row>
            </Grid>
        );
    }

    private getForm() {
        return (
            <form onSubmit={this.onSubmit}>

                <Username
                    value={this.state.username.value}

                    // we pass universal state updating handler down
                    // and the component will decide if and when validation should be run
                    updateFormState={this.updateFormState('username')}
                />

                <Password
                    value={this.state.password.value}
                    updateFormState={this.updateFormState('password')}
                />

                {this.state.isLoginErrorVisible && generateErrorMessages(Login.LOGIN_ERROR)}

                <input className="btn btn-primary pull-right" type="submit" value="Submit"/>

            </form>
        );
    }

    private async onSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();

        const isFormInvalid = this.state.username.errors.length > 0 || this.state.password.errors.length > 0;
        const isAnyFieldEmpty = !this.state.username.value || !this.state.password.value;

        if (isFormInvalid || isAnyFieldEmpty) {
            this.setState({isLoginErrorVisible: true});
            return;
        }

        const userData = {
            password: this.state.password.value,
            username: this.state.username.value,
        };

        await SessionService.openSession(userData);
        this.setState({isUserLoggedIn: true});
    }

    private updateFormState(field: 'password' | 'username') {
        return (event: React.FormEvent<FormControlProps>, errors: ValidationResult[] = []) => {
            const {value} = event.currentTarget;

            const newPartialState = {isLoginErrorVisible: false};
            newPartialState[field] = {
                errors,
                value,
            };

            this.setState(newPartialState);
        };
    }

}
