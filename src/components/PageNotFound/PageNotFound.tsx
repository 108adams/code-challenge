import * as React from 'react';
import {Col, Grid, Jumbotron, Row} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import './PageNotFound.less';

export const PageNotFound = () =>
    (
        <Grid className="page-not-found">
            <Row className="show-grid">
                <Col xs={12}>

                    <Jumbotron>
                        <h1>Page not found</h1>

                        <Link className="btn btn-primary" to="/">Go back to main page</Link>
                    </Jumbotron>

                </Col>
            </Row>
        </Grid>
    );
