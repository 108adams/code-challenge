import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {connect, Provider} from 'react-redux';
import configureStore, {MockStoreCreator, MockStoreEnhanced} from 'redux-mock-store';

import {Posts} from './Posts';

const ComonentWithStore = (ConnectedComponent: any, store: MockStoreEnhanced<any, any>) =>
    () => (
        <Provider store={store}>
            <ConnectedComponent/>
        </Provider>
    );

const storeCreator: MockStoreCreator = configureStore();
const initialEmptyState = {};
const Element = connect()(Posts);

// An example test of Redux-connected component
describe('Root component', () => {

    it('renders without crashing', () => {
        // GIVEN
        const div = document.createElement('div');
        const store: MockStoreEnhanced<{}, {}> = storeCreator({...initialEmptyState});
        const ConnectedElement = ComonentWithStore(Element, store);

        // THEN
        ReactDOM.render(<ConnectedElement/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});
