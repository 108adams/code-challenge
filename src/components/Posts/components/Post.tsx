import * as React from 'react';
import {Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import {Post as PostType} from '../../../interfaces/Post';

import './Post.less';

export const Post = ({id, userId, title}: PostType) =>
    (
        <Col className="wall-post" xs={12} md={6} lg={4}>
            <article>
                <div className="user-id">UserId: {userId}</div>

                <h5><Link className="title" to={`/post/${id}`}>Title: {title}</Link></h5>
            </article>
        </Col>
    );
