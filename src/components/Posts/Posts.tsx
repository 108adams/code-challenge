import * as React from 'react';
import {Row} from 'react-bootstrap';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';

import {Actions} from '../../reducers/Actions';
import {fetchPosts} from '../../services/ConnectorService';
import {getKey} from '../../utils/uniqueKeyGenerator';

import {Action} from '../../interfaces/Action';
import {Post} from '../../interfaces/Post';
import {State as ReduxState} from '../../interfaces/State';

import {Post as PostComponent} from './components/Post';

interface DispatchProps {
    clearPosts: () => Action;
    clearSearchState: () => Action;
    showPostsFetchingError: (error: string) => Action;
    updatePosts: (posts: Post[]) => Action;
}

interface StateProps {
    posts: Post[];
}

class Component extends React.Component<StateProps & DispatchProps, {}> {

    constructor(props: StateProps & DispatchProps) {
        super(props);

        this.props.clearSearchState();
        this.props.clearPosts();
    }

    public async componentDidMount() {
        // should be moved to a saga when scenario grows, but KISS for the moment
        // we load new list ON EVERY MOUNT - no caching policy specified
        try {
            const posts: Post[] = await fetchPosts();
            this.props.updatePosts(posts);
        } catch (fetchingError) {
            this.props.showPostsFetchingError(
                `We are sorry, but we were not able to get the list: ${fetchingError.message}`,
            );
        }
    }
    public componentWillUnmount() {
        this.props.clearPosts();
    }

    public render() {
        const posts = this.props.posts &&
            this.props.posts
                .filter((post) => post.isVisible)
                .filter((post) => !post.isDelayed)
                .map((post) => <PostComponent {...post} key={getKey()}/>);

        return (
            <Row className="show-grid">

                {posts}

            </Row>
        );
    }

}

const mapStateToProps = (state: ReduxState): StateProps => ({
    posts: state.posts,
});

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
    clearPosts: () => dispatch({type: Actions.CLEAR_POSTS}),
    clearSearchState: () => dispatch({type: Actions.SEARCH, value: ''}),
    showPostsFetchingError: (error) => dispatch({type: Actions.REPORT_APPLICATION_ERROR, value: error}),
    updatePosts: (posts) => dispatch({type: Actions.UPDATE_POSTS, value: posts}),
});

export const Posts = connect(mapStateToProps, mapDispatchToProps)(Component);
