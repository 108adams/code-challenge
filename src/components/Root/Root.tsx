import * as React from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {Store} from 'redux';

import {Login} from '../Login/Login';
import {PageNotFound} from '../PageNotFound/PageNotFound';
import {RestrictedPost} from '../SinglePost/RestrictedPost';
import {Logout} from '../Wall/components/Logout';
import {RestrictedWallRoot} from '../Wall/RestrictedWallRoot';

export const Root = ({store}: { store: Store }) =>
    (
        <Provider store={store}>
            <BrowserRouter>
                <Switch>
                    <Route exact={true} path="/" component={RestrictedWallRoot}/>
                    <Route path="/login" component={Login}/>
                    <Route path="/logout" component={Logout}/>
                    <Route path="/post/:id" component={RestrictedPost}/>
                    <Route component={PageNotFound}/>
                </Switch>
            </BrowserRouter>
        </Provider>
    );
