import * as React from 'react';
import * as ReactDOM from 'react-dom';
import configureStore, {MockStoreCreator} from 'redux-mock-store';

import {Root} from './Root';

describe('Root component', () => {

    const storeCreator: MockStoreCreator = configureStore();

    it('renders without crashing', () => {
        // GIVEN
        const div = document.createElement('div');

        // THEN
        ReactDOM.render(<Root store={storeCreator({})}/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});
