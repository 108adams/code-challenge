import * as React from 'react';
import {Redirect} from 'react-router';

import {isSessionOpen} from '../../services/SessionService';

export class RestrictedAccessArea extends React.Component {
    public render() {
        return isSessionOpen()
            ? this.props.children
            : <Redirect to={{pathname: '/login'}}/>;
    }
}
