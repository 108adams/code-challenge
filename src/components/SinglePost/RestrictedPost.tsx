import * as React from 'react';
import {match as Match} from 'react-router';

import {RestrictedAccessArea} from '../RestrictedAccessArea/RestrictedAccessArea';
import {Post} from './components/Post';

export const RestrictedPost = ({match}: { match: Match<{ id: string; }> }) => {
    const id = parseInt(match.params.id, 10);

    return (
        <RestrictedAccessArea>
            <Post id={id}/>
        </RestrictedAccessArea>
    );
};
