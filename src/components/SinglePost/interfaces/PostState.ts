import {Post as PostType} from '../../../interfaces/Post';

export interface PostState extends PostType {
    applicationError: string;
}
