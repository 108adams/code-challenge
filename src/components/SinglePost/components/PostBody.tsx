import * as React from 'react';

import {PostState} from '../interfaces/PostState';

import './PostBody.less';

export const PostBody = ({id, userId, title, body}: PostState) => (
    <article>
        <div className="user-id">UserId: {userId}</div>

        <div className="id">Id: {id}</div>

        <div className="title">Title: {title}</div>

        <p className="body">
            Body: {body}
        </p>
    </article>
);
