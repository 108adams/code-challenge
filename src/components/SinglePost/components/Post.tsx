import * as React from 'react';
import {Grid, Row} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import {fetchPost} from '../../../services/ConnectorService';

import {Post as PostType} from '../../../interfaces/Post';
import {PostState} from '../interfaces/PostState';

import {Component as ApplicationError} from '../../ApplicationError/ApplicationError';
import {PostBody} from './PostBody';

import './Post.less';

interface PostProps {
    id: number;
}

export class Post extends React.Component<PostProps, PostState> {

    constructor(props: PostProps) {
        super(props);

        // it is required to fetch post data from an endpoint despite the fact
        // we might have already filled store with data (if the user had visited the Posts Wall before)
        //
        // this is why we do not connect to store at all here and use internal state
        this.state = {
            id: 0,
            userId: 0,

            body: '',
            title: '',

            isDelayed: false,
            isVisible: true,

            applicationError: '',
        };
    }

    public async componentDidMount() {
        // should be moved to a saga when scenario grows, but KISS for the moment
        try {
            const post: PostType = await fetchPost(this.props.id);
            this.setState(post);
        } catch (fetchingError) {
            this.setState({
                applicationError: fetchingError.message,
            });
        }
    }

    public render() {
        return (
            <Grid>
                <Row className="show-grid">

                    {this.getError()}

                    {this.getArticle()}

                </Row>
            </Grid>
        );
    }

    private getError() {
        const {applicationError} = this.state;

        return applicationError && <Error error={applicationError}/>;
    }

    private getArticle() {
        const hasDataLoaded = this.state.id !== 0;

        return hasDataLoaded && (
            <div className="single-post col-xs-12">

                <Link className="btn btn-primary" to="/">Go back</Link>

                <div className="details">Details</div>

                <PostBody {...this.state}/>
            </div>
        );
    }
}

const Error = ({error}: { error: string }) => (
    <ApplicationError
        applicationError={`We have encountered an error: ${error}`}
        exitLink="/"
        exitLabel="Go back to the list"
    />
);
