import * as React from 'react';
import {Grid} from 'react-bootstrap';

import {ApplicationError} from '../../ApplicationError/ApplicationError';
import {Posts} from '../../Posts/Posts';
import {Header} from './Header';

export const Wall = () =>
    (
        <Grid>

            <ApplicationError exitLink="/" exitLabel="Try again"/>

            <Header/>

            <Posts/>

        </Grid>
    );
