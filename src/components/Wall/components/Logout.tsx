import * as React from 'react';
import {Button, Col} from 'react-bootstrap';
import {Redirect} from 'react-router';

import {closeSession} from '../../../services/SessionService';

import './Logout.less';

export class Logout extends React.Component<{}, { isLoggedOut: boolean }> {

    constructor(props: any) {
        super(props);

        this.state = {
            isLoggedOut: false,
        };

        this.onClickHandler = this.onClickHandler.bind(this);
    }

    public render() {
        return (
            <Col className="logout" xs={12} md={6} lg={4}>
                <Button
                    bsStyle="primary"
                    onClick={this.onClickHandler}
                >
                    Log out
                </Button>

                {this.state.isLoggedOut && <Redirect to="/"/>}
            </Col>
        );
    }

    private async onClickHandler() {
        await closeSession();

        this.setState({isLoggedOut: true});
    }

}
