import * as React from 'react';
import {Col, FormControl, FormControlProps} from 'react-bootstrap';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';

import {Action} from '../../../interfaces/Action';
import {State as ReduxState} from '../../../interfaces/State';
import {Actions} from '../../../reducers/Actions';

import './Search.less';

interface SearchProps {
    pattern: string;
}

interface DispatchProps {
    search: (event: React.FormEvent<FormControlProps>) => Action;
}

const Component = ({pattern, search}: SearchProps & DispatchProps) =>
    (
        <Col className="search" xs={12} md={6} lg={8}>
            <FormControl
                type="text"
                placeholder="search"
                value={pattern}
                onChange={search}
            />
        </Col>
    );

const mapStateToProps = (state: ReduxState): SearchProps => ({
    pattern: state.pattern,
});

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
    search: (event) => dispatch({type: Actions.SEARCH, value: event.currentTarget.value}),
});

export const Search = connect(mapStateToProps, mapDispatchToProps)(Component);
