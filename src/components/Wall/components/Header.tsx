import * as React from 'react';
import {Row} from 'react-bootstrap';

import {Logout} from './Logout';
import {Search} from './Search';

import './Header.less';

export const Header = () =>
    (
        <Row className="show-grid wall-header">

            <Logout/>

            <Search/>

        </Row>
    );
