import * as React from 'react';

import {RestrictedAccessArea} from '../RestrictedAccessArea/RestrictedAccessArea';
import {Wall} from './components/Wall';

export const RestrictedWallRoot = () =>
    (
        <RestrictedAccessArea>
            <Wall/>
        </RestrictedAccessArea>
    );
