import {Post} from '../interfaces/Post';
import {RawPost} from '../interfaces/RawPost';

export const prepareDataForWall = (source: RawPost): Post => {
    const isDelayed = true;
    const isVisible = true;
    return {...source, isDelayed, isVisible}; // spread fools TS, we need to be very cautious
};

export const prepareDataForSinglePost = (source: RawPost): Post => {
    const isDelayed = false;
    const isVisible = true;
    return {...source, isDelayed, isVisible}; // spread fools TS, we need to be very cautious
};

export const makeVisible = (source: Post): Post => {
    const isVisible = true;
    return {...source, isVisible};
};

export const makeInvisible = (source: Post): Post => {
    const isVisible = false;
    return {...source, isVisible};
};

export const makeDelayed = (source: Post): Post => {
    const isDelayed = true;
    return {...source, isDelayed};
};

export const makeInstant = (source: Post): Post => {
    const isDelayed = false;
    return {...source, isDelayed};
};
