import * as React from 'react';
import {Alert} from 'react-bootstrap';

import {getKey} from './uniqueKeyGenerator';

export const  generateErrorMessages = (errors: string[]) =>
    errors.map((error) =>
        (
            <Alert bsStyle="warning" key={getKey()}>
                {error}
            </Alert>
        ),
    );
