import {Post} from '../interfaces/Post';
import {search} from './FullTextSearchService';

// this one was TDD based
describe('FullTextSearchService', () => {

    const posts: Post[] = [{
        id: 1,
        userId: 1,

        body: 'oooooa',
        title: 'ooooob',

        isVisible: true,
    }, {
        id: 2,
        userId: 2,

        body: 'oooooc',
        title: 'ooooob',

        isVisible: true,
    }, {
        id: 3,
        userId: 3,

        body: 'oooood',
        title: 'oooooe',

        isVisible: true,
    }];

    describe('search', () => {
        it('should find pattern in a longer text', () => {
            // GIVEN
            const pattern = 'a';

            // WHEN
            const updatedPosts = search(pattern, posts);
            const visiblePosts = updatedPosts.filter((post) => post.isVisible);

            // THEN
            expect(visiblePosts.length).toEqual(1);
            expect(visiblePosts[0].id).toEqual(1);
        });

        it('should mark non-matching posts as invisible', () => {
            // GIVEN
            const pattern = 'a';

            // WHEN
            const updatedPosts = search(pattern, posts);
            const notVisiblePosts = updatedPosts.filter((post) => !post.isVisible);

            // THEN
            expect(notVisiblePosts.length).toEqual(2);
            expect(notVisiblePosts[0].id).toEqual(2);
            expect(notVisiblePosts[1].id).toEqual(3);
        });

        it('should search in title', () => {
            // GIVEN
            const pattern = 'b';

            // WHEN
            const updatedPosts = search(pattern, posts);
            const visiblePosts = updatedPosts.filter((post) => post.isVisible);

            // THEN
            expect(visiblePosts.length).toEqual(2);
            expect(visiblePosts[0].id).toEqual(1);
            expect(visiblePosts[1].id).toEqual(2);
        });

        it('should search in body', () => {
            // GIVEN
            const pattern = 'd';

            // WHEN
            const updatedPosts = search(pattern, posts);
            const visiblePosts = updatedPosts.filter((post) => post.isVisible);

            // THEN
            expect(visiblePosts.length).toEqual(1);
            expect(visiblePosts[0].id).toEqual(3);
        });

        it('should handle empty pattern: all posts should be visible', () => {
            // GIVEN
            const pattern = '';

            // WHEN
            const updatedPosts = search(pattern, posts);
            const visiblePosts = updatedPosts.filter((post) => post.isVisible);

            // THEN
            expect(visiblePosts.length).toEqual(3);
            expect(visiblePosts[0].id).toEqual(1);
            expect(visiblePosts[1].id).toEqual(2);
            expect(visiblePosts[2].id).toEqual(3);
        });

        it('should handle non-matching pattern: no post should be visible', () => {
            // GIVEN
            const pattern = 'z';

            // WHEN
            const updatedPosts = search(pattern, posts);
            const visiblePosts = updatedPosts.filter((post) => post.isVisible);

            // THEN
            expect(visiblePosts.length).toEqual(0);
        });

    });
});
