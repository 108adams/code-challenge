import {Post} from '../interfaces/Post';

export const search = (pattern: string, posts: Post[]): Post[] =>
    posts.map(markInvisibleIfNotContainsPattern(pattern));

const markInvisibleIfNotContainsPattern = (pattern: string) =>
    (post: Post): Post => {
        const {title, body} = post;

        const containsPattern = (
            isEmpty(pattern) ||
            contains(title, pattern) ||
            contains(body, pattern)
        );

        return containsPattern
            ? {...post, isVisible: true}
            : {...post, isVisible: false};
    };

const contains = (text: string, pattern: string): boolean =>
    text.indexOf(pattern) !== -1;

const isEmpty = (pattern: string): boolean =>
    pattern.length === 0;
