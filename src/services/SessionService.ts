import {UserData} from './interfaces/UserData';

const SESSION_KEY = 'session';

export async function openSession(userData: UserData) {
    await cleanSession();

    const sessionData = JSON.stringify(userData);
    window.sessionStorage.setItem(SESSION_KEY, sessionData);
}

export function isSessionOpen(): boolean {
    return window.sessionStorage.getItem(SESSION_KEY) !== null;
}

export async function closeSession() {
    await cleanSession();
}

async function cleanSession() {
    window.sessionStorage.removeItem(SESSION_KEY);
}
