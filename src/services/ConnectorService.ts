import {Post} from '../interfaces/Post';
import {RawPost} from '../interfaces/RawPost';
import {prepareDataForSinglePost, prepareDataForWall} from '../utils/postVisibilityHandlers';

const POSTS_URL = 'https://jsonplaceholder.typicode.com/posts';

// We do not perform any schema validation here,
// or potential XSS handling
// requirements imply that we trust the source

export async function fetchPosts(): Promise<Post[]> {
    const response: Response = await fetch(POSTS_URL);
    return response
        .json()
        // set default values on visibility and delay
        .then((posts: RawPost[]): Post[] => posts.map(prepareDataForWall));
}

export async function fetchPost(id: number): Promise<Post> {
    const response: Response = await fetch(`${POSTS_URL}/${id}`);
    return response
        .json()
        .then((post: RawPost): Post => prepareDataForSinglePost(post));
}
