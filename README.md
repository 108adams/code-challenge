# Front end code challenge

A simple social networking application, similar to Twitter, using publicly available API.

Reference specification: https://github.com/HStoneAge/code-challenge/blob/master/README-FRONTEND.md


## Installation

Run `npm install` in main project's directory to install all dependencies.


## Testing

The code includes some example tests. The coverage is far from 100%, as it's just an example. Standard React test environment is used, with _Jest_. 

Run tests with `npm test`.


## Configuration

The project is based on `react-create-app`, but it was "ejected": the config files were moved out from _node_modules_ to the project itself. LESS support was added and TS config updated to handle transpilation of `yield*` (see _delaySaga_ for the code).

**WARNING** As the project configuration is not a standard `react-create-app` application any more, it may need manual update of _package.info_. See line 133 (`tsConfigFile`) if tests don't run properly.


## Building

To run dev server use `npm start`. It will start a server on _http://localhost:3000_.


## Deployed app

To see the app deployed, visit https://adams-c0429.firebaseapp.com/login


_Disclaimer_ this code was created for HSBC recruitment purposes only. All rights reserved by Adam Kucharczyk. No licence is granted to copy or reuse the code without permission.

Copyright (C) 2018 by Adam Kucharczyk.